import * as plugins from './plugins.js';

import { css, html } from '@designestate/dees-element';

declare global {
  interface HTMLElementTagNameMap {
    'dedocs-documentmode-footer': DedocsDocumentmodeFooter;
  }
}

@plugins.deesElement.customElement('dedocs-documentmode-footer')
export class DedocsDocumentmodeFooter extends plugins.deesElement.DeesElement {
  public static demo = () => plugins.deesElement.html`
    <dedocs-documentmode-footer .aProp="${'test'}"></dedocs-documentmode-footer>
  `;

  @plugins.deesElement.property({
    type: String
  })
  public aProp: string = 'loading...';


  constructor() {
    super();
  }

  public static styles = [
    plugins.domtools.elementBasic.staticStyles,
    css`
      .mainbox {
        font-family: 'Exo 2', sans-serif;
        display: block;
        background: #007EDA;
        color: white;
        text-align: center;
        height: 25px;
        overflow: hidden;
        padding: 2px;
        transition: all 0.3s;
        position: absolute;
        bottom: 0px;
        right: 0px;
        left: 0px;
      }

      .mainbox.documentMode {
        height: 70px;
      }
    `
  ]

  public render() {
    return html`
      <div @click="${() => {this.toggleDocumentMode()}}" class="mainbox ${this.documentMode ? 'documentMode': ''}">
        <div class="innerHeader ${this.documentMode ? 'documentMode': ''}">
        
        
        </div>
        <div class="slogan ${this.documentMode ? 'documentMode': ''}">Easy TypeScript Modules for Complex Problems</div>
      </div>
    `;
  }

  @plugins.deesElement.property()
  public documentMode: boolean = false;
  
  public toggleDocumentMode(documentModeArg: boolean = !this.documentMode) {
    this.documentMode = documentModeArg;
  }
}