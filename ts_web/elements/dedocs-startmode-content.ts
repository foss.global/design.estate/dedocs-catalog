import * as plugins from './plugins.js';

import { css, html } from '@designestate/dees-element';

declare global {
  interface HTMLElementTagNameMap {
    'dedocs-startmode-content': DedocsStartmodeContent;
  }
}

@plugins.deesElement.customElement('dedocs-startmode-content')
export class DedocsStartmodeContent extends plugins.deesElement.DeesElement {
  public static demo = () => plugins.deesElement.html`
    <dedocs-startmode-content .aProp="${'test'}"></dedocs-startmode-content>
  `;

  @plugins.deesElement.property({
    type: String,
  })
  public aProp: string = 'loading...';

  constructor() {
    super();
  }

  public static styles = [
    plugins.domtools.elementBasic.staticStyles,
    css`
      .mainbox {
        font-family: 'Exo 2', sans-serif;
        display: block;
        background: #000000;
        color: white;
        transition: all 0.3s;
      }

      .mainbox.documentMode {
      }

      .content {
        padding: 30px 0px;
        max-width: 1000px;
        margin: auto;
      }

      h1 {
        font-weight: 400;
        font-size: 22px;
        border-bottom: 1px solid #555555;
        margin: 0px;
        padding: 10px 0px;
      }

      .articleSelectionGrid {
        display: flex;
        gap: 10px;
        padding: 20px 0px;
        flex-wrap: wrap;
      }
      .article {
        height: 110px;
        min-width: 100px;
        border: 1px solid #555555;
        border-radius: 3px;
      }
    `,
  ];

  public render() {
    return html`
      <div
        @click="${() => {
          this.toggleDocumentMode();
        }}"
        class="mainbox ${this.documentMode ? 'documentMode' : ''}"
      >
        <div class="content">
          <h1>Featured Modules</h1>
          <div class="articleSelectionGrid">
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
          </div>
          <h1>Latest Updated Modules</h1>
          <div class="articleSelectionGrid">
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
            <div class="article"></div>
          </div>
        </div>
      </div>
    `;
  }

  @plugins.deesElement.property()
  public documentMode: boolean = false;

  public toggleDocumentMode(documentModeArg: boolean = !this.documentMode) {
    this.documentMode = documentModeArg;
  }
}
