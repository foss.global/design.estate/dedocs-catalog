import * as plugins from './plugins.js';

import { css, html } from '@designestate/dees-element';

declare global {
  interface HTMLElementTagNameMap {
    'dedocs-header': DedocsHeader;
  }
}

@plugins.deesElement.customElement('dedocs-header')
export class DedocsHeader extends plugins.deesElement.DeesElement {
  public static demo = () => plugins.deesElement.html`
    <dedocs-header .aProp="${'test'}"></dedocs-header>
  `;

  @plugins.deesElement.property({
    type: String
  })
  public aProp: string = 'loading...';


  constructor() {
    super();
  }

  public static styles = [
    plugins.domtools.elementBasic.staticStyles,
    css`
      .mainbox {
        font-family: 'Exo 2', sans-serif;
        display: block;
        background: #282828;
        color: white;
        text-align: center;
        height: 200px;
        overflow: hidden;
        transition: all 0.3s;
      }

      .mainbox.documentMode {
        height: 70px;
      }

      .innerHeader {
        transition: all 0.3s;
        height: 70px;
        border-bottom: 1px solid #555555;
        max-width: 1000px;
        margin:auto;
        display: grid;
        padding: 0px 0px 0px 0px;
        grid-template-columns: 200px auto 200px;
      }
      .innerHeader.documentMode {
        max-width: 100vw;
        padding: 0px 10px 0px 10px;
      }

      .searchBox {
        border: 1px solid #555555;
        height: 40px;
        margin-top: 15px;
        border-radius: 3px;
      }
      .slogan {
        transition: all 0.2s;
        opacity: 1;
        padding: 50px;
        font-size: 26px;
        text-align: center;
        margin: auto;
        max-width: 1000px;
      }
      .slogan.documentMode {
        opacity: 0;
      }
    `
  ]

  public render() {
    return plugins.deesElement.html`
      <div @click="${() => {this.toggleDocumentMode()}}" class="mainbox ${this.documentMode ? 'documentMode': ''}">
        <div class="innerHeader ${this.documentMode ? 'documentMode': ''}">
          <div></div>
          <div></div>
          <div class="searchBox"></div>
        </div>
        <div class="slogan ${this.documentMode ? 'documentMode': ''}">Easy TypeScript Modules for Complex Problems</div>
      </div>
    `;
  }

  @plugins.deesElement.property()
  public documentMode: boolean = false;
  
  public toggleDocumentMode(documentModeArg: boolean = !this.documentMode) {
    this.documentMode = documentModeArg;
  }
}